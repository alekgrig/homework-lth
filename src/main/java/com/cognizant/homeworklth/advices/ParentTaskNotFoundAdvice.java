package com.cognizant.homeworklth.advices;

import com.cognizant.homeworklth.exceptions.ParentTaskNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ParentTaskNotFoundAdvice {

    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ParentTaskNotFoundException.class)
    String parentTaskNotFoundHandler(ParentTaskNotFoundException ex) {
        return ex.getMessage();
    }

}