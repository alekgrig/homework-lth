package com.cognizant.homeworklth.services;

import com.cognizant.homeworklth.entities.Task;
import com.cognizant.homeworklth.exceptions.ParentTaskNotFoundException;
import com.cognizant.homeworklth.exceptions.TaskNotFoundException;
import com.cognizant.homeworklth.repositories.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    public Task get(Long id) {
        return taskRepository.findById(id).orElseThrow(() -> new TaskNotFoundException(id));
    }

    public ResponseEntity<Task> create(Task task) {
        if (task.getParent_id() != null && task.getParent_id() != 0) {
            Optional<Task> parentTask = taskRepository.findById(task.getParent_id());
            if (!parentTask.isPresent()) {
                throw new ParentTaskNotFoundException(task.getParent_id());
            }
            task.setParent(parentTask.get());
        }
        Task newTask = taskRepository.save(task);
        return ResponseEntity.ok(newTask);
    }

    public ResponseEntity<Task> update(long id, Task taskData) {
        Optional<Task> updatedTask = taskRepository.findById(id);

        if (updatedTask.isPresent()) {
            if (taskData.getParent_id() != null && taskData.getParent_id() != 0) {
                Optional<Task> parentTask = taskRepository.findById(taskData.getParent_id());
                if (!parentTask.isPresent()) {
                    throw new ParentTaskNotFoundException(taskData.getParent_id());
                }
                taskData.setParent(parentTask.get());
            }

            updatedTask.map(task -> {
                task.setName(taskData.getName());
                task.setSecondsSpentOnTask(taskData.getSecondsSpentOnTask());
                task.setTaskGroupId(taskData.getTaskGroupId());
                task.setTaskAssigneeId(taskData.getTaskAssigneeId());
                task.setParent(taskData.getParent());
                task.setFinished(taskData.isFinished());
                return taskRepository.save(task);
            });

            return ResponseEntity.ok(updatedTask.get());
        } else {
            throw new TaskNotFoundException(id);
        }

    }

    public void delete(long id) {
        taskRepository.deleteById(id);
    }

}
