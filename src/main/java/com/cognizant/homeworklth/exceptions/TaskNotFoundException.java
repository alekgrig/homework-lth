package com.cognizant.homeworklth.exceptions;

public class TaskNotFoundException extends RuntimeException {

    public TaskNotFoundException(long id) {
        super("Could not find task with ID: " + id);
    }

}