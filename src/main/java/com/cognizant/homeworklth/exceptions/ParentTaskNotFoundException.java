package com.cognizant.homeworklth.exceptions;

public class ParentTaskNotFoundException extends RuntimeException {

    public ParentTaskNotFoundException(long id) {
        super("Could not find Parent task with ID: " + id);
    }
}
