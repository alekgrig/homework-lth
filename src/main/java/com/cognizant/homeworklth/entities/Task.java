package com.cognizant.homeworklth.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Task {

    @Id
    @Getter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    @ApiModelProperty(readOnly = true)
    private Long id;

    @Setter
    @Getter
    private String name;

    @Setter
    @Getter
    private int secondsSpentOnTask;

    @Setter
    @Getter
    private int taskGroupId;

    @Setter
    @Getter
    private int taskAssigneeId;

    @Setter
    @Getter
    private boolean isFinished;

    @Getter
    @Transient
    private Long parent_id = null;

    @Setter
    @Getter
    @ManyToOne
    @JsonBackReference
    private Task parent;

    @Getter
    @JsonManagedReference
    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, orphanRemoval = true)
    @ApiModelProperty(readOnly = true)
    private List<Task> tasks = new ArrayList<>();

}
