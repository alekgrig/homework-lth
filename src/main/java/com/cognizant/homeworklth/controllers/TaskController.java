package com.cognizant.homeworklth.controllers;

import com.cognizant.homeworklth.entities.Task;
import com.cognizant.homeworklth.services.TaskService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/tasks")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @GetMapping("/{id}")
    @ApiOperation(
            value = "Find task by id",
            notes = "Provide an id to look up specific task from existing tasks"
    )
    public Task task(@PathVariable long id) {
        return taskService.get(id);
    }

    @PostMapping
    @ApiOperation(
            value = "Add new task",
            notes = "Create new tasks with data you passed in"
    )
    public ResponseEntity<Task> createTask(@RequestBody Task task) {
        return taskService.create(task);
    }

    @PutMapping("/{id}")
    @ApiOperation(
            value = "Update existing task",
            notes = "Provide an id of task you want to update and pass new data to update it"
    )
    public ResponseEntity<Task> updateTask(@RequestBody Task task, @PathVariable int id) {
        return taskService.update(id, task);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(
            value = "Delete existing task by id",
            notes = "Deletes task and it sub-tasks by specified id"
    )
    public void deleteTask(@PathVariable int id) {
        taskService.delete(id);
    }

}
