package com.cognizant.homeworklth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@SpringBootApplication
@EnableJpaRepositories
@EnableSwagger2
public class HomeworklthApplication {

    public static void main(String[] args) {
        SpringApplication.run(HomeworklthApplication.class, args);
    }

    @Bean
    public Docket swaggerConfig() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.ant("/api/**"))
                .apis(RequestHandlerSelectors.basePackage("com.cognizant"))
                .build()
                .apiInfo(apiDetails());
    }

    private ApiInfo apiDetails() {
        return new ApiInfo(
                "Tasks API",
                "Homework for Cognizant LTH",
                "1.0",
                null,
                new springfox.documentation.service.Contact("Aleksandrs Grigorjevs", "http://www.dailyalex.com", "email@me.com"),
                "MIT License",
                "https://opensource.org/licenses/MIT",
                Collections.emptyList()
        );
    }

}
