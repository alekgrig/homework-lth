package com.cognizant.homeworklth.repositories;

import com.cognizant.homeworklth.entities.Task;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public interface TaskRepository extends CrudRepository<Task, Long> {
}